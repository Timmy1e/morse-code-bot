import setuptools

with open("README.md", "r") as readme_file:
    readme_text = readme_file.read()

setuptools.setup(
    name="morseCodeBot",
    version="1.1.0",
    author="Tim van Leuverden",
    author_email="TvanLeuverden@Gmail.com",
    description="A Telegram morse code bot",
    long_description=readme_text,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Timmy1e/morse-code-bot",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
        'setuptools~=50.3.2',
        'telepot~=12.7'
    ],
    license="GPL-3",
    entry_points={
        'console_scripts': [
            'morseCodeBot=morseCodeBot.main:main'
        ]
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent"
    ],
)
