FROM python:3.9

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .

# Install
RUN pip install --no-cache-dir --disable-pip-version-check .

# Start app
CMD [ "morseCodeBot" ]
