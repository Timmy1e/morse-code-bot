# Morse code bot
A Telegram morse code converter bot.

The image uses the following environment variables:
 - `TELEGRAM_KEY` this is the secret key for the Telegram account 
 - `TELEGRAM_ID` this is the ID to send the startup message to
 - `LOG_LEVEL` this is the level for logging, can be `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL` (default)

