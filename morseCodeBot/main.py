import os
import sys
import asyncio
import telepot

from telepot.aio.loop import MessageLoop
from telepot.aio.delegate import per_inline_from_id, per_chat_id, create_open, pave_event_space

from .util import log
from .chat_handler import MorseCodeChatHandler
from .inline_handler import MorseCodeInlineHandler

TELEGRAM_ID = 'TELEGRAM_ID'
TELEGRAM_KEY = 'TELEGRAM_KEY'


# Main function.
def main():
    # Read the API key from telegram.
    if TELEGRAM_KEY not in os.environ:
        log.critical('Environment key "%s" missing, please set it.' % TELEGRAM_KEY)
        sys.exit(1)
    telegram_key = os.getenv(TELEGRAM_KEY).strip()

    # Read the ID of the user to send a message to on start, can be left empty.
    if TELEGRAM_ID not in os.environ:
        log.warn('Environment key "%s" missing, will not send a starting message.' % TELEGRAM_ID)
    telegram_id = os.getenv(TELEGRAM_ID, '').strip()

    # Initialize the bot.
    bot = telepot.aio.DelegatorBot(telegram_key, [
        pave_event_space()(
            per_inline_from_id(), create_open, MorseCodeInlineHandler, timeout=5
        ),
        pave_event_space()(
            per_chat_id(), create_open, MorseCodeChatHandler, timeout=5
        )
    ])

    # Start the loop
    loop = asyncio.get_event_loop()
    loop.create_task(MessageLoop(bot).run_forever())

    if telegram_id:
        # Send a message to show it is running.
        asyncio.get_event_loop().run_until_complete(
            bot.sendMessage(telegram_id, "`I'm running...`", parse_mode='Markdown')
        )

    # Log that it it listening
    log.info('Listening ...')

    # Sleep, as the service runs async.
    loop.run_forever()
