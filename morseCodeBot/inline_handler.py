import re
import telepot
from telepot.aio.helper import InlineUserHandler, AnswererMixin

from .util import generate_id, log
from .converter import MorseCodeConverter


class MorseCodeInlineHandler(InlineUserHandler, AnswererMixin):
    def __init__(self, *args, **kwargs):
        self.morse = MorseCodeConverter()
        super(MorseCodeInlineHandler, self).__init__(*args, **kwargs)

    # Handle inline query messages.
    def on_inline_query(self, msg):
        query_id, from_id, query_string = telepot.glance(msg, flavor='inline_query')
        log.debug("Inline Query: %s, %s, %s;" % (query_id, from_id, query_string))

        # Method to calculate answer for the inline query.
        def compute_answer():
            result_id = str(generate_id())

            # Get values for to and from
            to_text = re.sub(r'[^a-zA-Z0-9 ]', r'', query_string).strip()
            from_text = re.sub(r'[^-./ ]', r'', query_string).strip()

            # Smart switch if it is to or from.
            if to_text and not to_text.isspace():
                # To
                result_text = to_text
                result_title = "Text translated to morse code."
                result_output = self.morse.to_morse(to_text)
            elif from_text and not from_text.isspace():
                # From
                result_text = from_text
                result_title = "Morse code translated to text."
                result_output = self.morse.from_morse(from_text)
            else:
                # Unknown
                result_text = query_string
                result_title = "Couldn't convert to or from morse code."
                result_output = query_string

            log.debug("Inline:\t'%s', '%s', '%s', '%s'" % (result_id, result_text, result_title, result_output))

            # Result
            return [{
                'type': "article",
                'id': result_id,
                'title': result_title,
                'description': "%s ⟶ %s" % (result_text, result_output),
                'message_text': result_output
            }]

        # Return values
        self.answerer.answer(msg, compute_answer)
        return
