import os
import re
import telepot
from telepot.aio.helper import ChatHandler

from .util import save_getter, log
from .converter import MorseCodeConverter


class MorseCodeChatHandler(ChatHandler):
    def __init__(self, seed_tuple, **kwargs):
        super(MorseCodeChatHandler, self).__init__(seed_tuple, **kwargs)
        self.replyToIds = []
        self.replyFromIds = []
        self.morse = MorseCodeConverter()
        self.name = "@MorseCodeBot"
        self.commands = {'help': "help", 'to': "to", 'from': "from"}
        self.forceReply = {'force_reply': True, 'selective': True}
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "help_text.md"), "r") as help_file:
            self.help_text = help_file.read()

    # Handle regular chat messages.
    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        text = save_getter(msg, 'text')
        msg_id = save_getter(msg, 'message_id')
        reply = save_getter(msg, 'reply_to_message', 'message_id')
        command = None

        # Is it a reply?
        if reply:
            log.debug("It's a reply: %s %s %s" % (reply, str(self.replyToIds), str(self.replyFromIds)))
            # First check reply to ID's.
            for replyId in self.replyToIds:
                if reply == replyId:
                    self.replyToIds.remove(replyId)
                    command = self.commands['to']
                    break

            # If not found yet, check reply from ID's.
            if not command:
                for replyId in self.replyFromIds:
                    if reply == replyId:
                        self.replyFromIds.remove(replyId)
                        command = self.commands['from']
                        break

        # It is not a reply, or it is no longer found.
        if not command and not text:
            # I don't know how to handle this
            return
        elif not command:
            for command_cmd in self.commands:
                full_cmd = "/%s" % command_cmd
                if text.find(full_cmd) != -1:
                    text = text[text.index(full_cmd) + len(full_cmd):]
                    command = command_cmd
                    break

        # Remove own name.
        if text.find(self.name) != -1:
            text = text[text.index(self.name) + len(self.name):]

        # Text is empty, ask for text in a reply.
        if (not text or text.isspace()) and (command != self.commands['help'] or not command):
            my_message = await self.sender.sendMessage("What should I convert %s morse code?" % command,
                                                       reply_to_message_id=msg_id, reply_markup=self.forceReply)
            if command == self.commands['to']:
                self.replyToIds.append(my_message['message_id'])
                log.debug('Put "%s" in my "to" pocket' % str(self.replyToIds))
            elif command == self.commands['from']:
                self.replyFromIds.append(my_message['message_id'])
                log.debug('Put "%s" in my "from" pocket' % str(self.replyFromIds))
            else:
                log.error("This shouldn't be happening, 'text is empty and no command found'!")
            return

        # We have some text, or a command witch needs no arguments.
        text = text.strip()
        text_to = re.sub(r'[^a-zA-Z0-9 ]', r'', text).strip()
        text_from = re.sub(r'[^-./ ]', r'', text).strip()

        # Handle the commands.
        if command == self.commands['to'] and not text_to.isspace():
            # /to
            output = self.morse.to_morse(text)
        elif command == self.commands['from'] and not text_from.isspace():
            # /from
            output = self.morse.from_morse(text)
        else:
            # Default, just return /help.
            output = self.help_text

        # Log some debug info.
        log.debug("> Normal Message: %s, %s, %s, '%s' > '%s' > '%s';" % (
            content_type, chat_type, chat_id, text, command, output))

        # Clean the output.
        output = output.strip()
        if not output or output.isspace():
            output = "**`Couldn't convert %s morse code.`**" % command

        # Send the message
        await self.sender.sendMessage(
            output,
            parse_mode="Markdown", disable_web_page_preview=True, reply_to_message_id=msg_id
        )
        return
