This is morse code conversion bot, works inline or through direct commands.

Direct command usage:
`  /to <text>  ` Convert text to morse code.
`  /from <text>` Convert morse code to text.
`  /help       ` Prints this help text.

Inline command usage:
`  <text>      ` Convert text to or from morse code, figures it out automatically.

Links:
[Issues](https://gitlab.com/Timmy1e/morse-code-bot/-/issues) [Source code](https://gitlab.com/Timmy1e/morse-code-bot) [Creator](https://leuverden.nl) [Support me](https://ko-fi.com/timmy1e)
