import os
import random
import logging

logging.basicConfig(format='%(levelname)s\t- %(message)s', level=os.environ.get('LOG_LEVEL', 'CRITICAL').upper())
log = logging.getLogger(__name__)
log.info('Starting...')


# Safely get the value from an object.
def save_getter(var_obj, var_id, var_id_sub=None, var_fail=None):
    try:
        if not var_id_sub:
            result = var_obj[var_id]
        else:
            result = var_obj[var_id][var_id_sub]
    except (KeyError, AssertionError):
        result = var_fail
    return result


# Generate an ID for inline queries.
def generate_id():
    id_length = 10
    range_start = 10 ** (id_length - 1)
    range_end = (10 ** id_length) - 1
    return random.randint(range_start, range_end)
