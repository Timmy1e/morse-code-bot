import re


# Class to convert to and from morse code.
class MorseCodeConverter:
    def __init__(self):
        self.morseAlphabet = {
            "A": ".-", "B": "-...", "C": "-.-.", "D": "-..", "E": ".",
            "F": "..-.", "G": "--.", "H": "....", "I": "..", "J": ".---",
            "K": "-.-", "L": ".-..", "M": "--", "N": "-.", "O": "---",
            "P": ".--.", "Q": "--.-", "R": ".-.", "S": "...", "T": "-",
            "U": "..-", "V": "...-", "W": ".--", "X": "-..-", "Y": "-.--",
            "Z": "--..", " ": "/", "1": ".----", "2": "..---", "3": "...--",
            "4": "....-", "5": ".....", "6": "-....", "7": "--...",
            "8": "---..", "9": "----.", "0": "-----"
        }
        self.inverseMorseAlphabet = {morse: letter for letter, morse in self.morseAlphabet.items()}

    def to_morse(self, msg):
        code = re.sub(r'[^a-zA-Z0-9 ]', r'', msg)
        return " ".join(self.morseAlphabet[char.upper()] for char in code)

    def from_morse(self, code):
        msg = re.sub(r'[^-./ ]', r'', code)
        return "".join(self.inverseMorseAlphabet[char] for char in msg.split())
